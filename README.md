# Text Editor

## What is it?
This project is a simple text editor as Notepad.
In this is a project I was try to use the i18n, bundles and other kind  of L&F.

## Documentation
I will try to write.

## Release notes
* Convert to Maven project
* Upgraded from JDK 1.6 to JDK 1.8
* License changed to Apache 2.0

##  System requirements

| Description      | Requirement                                               |
| -----------------|:---------------------------------------------------------:|
| JDK              | 1.8                                                       |
| Memory           | N/A                                                       |
| Disk             | N/A                                                       |
| Operating System | Windows, Unix-based systems (Linux, Solaris and Mac OS X) |

##  Install
  1. Clone the project or download the source code.
  2. Go to source code folder.
  3. Using [Maven](https://maven.apache.org/)  execute `mvn clean install`.
  4. Open a terminal and go to source code folder and then target folder.
  5. And run `java -cp texteditor-1.0-SNAPSHOT.jar mx.neocs.texteditor.Main`.

## License
Please look at [LICENSE](LICENSE.md) file.
