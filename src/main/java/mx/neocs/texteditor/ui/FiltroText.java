/*
 * Copyright 2016 Freddy Barrera (freddy.barrera.moo@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mx.neocs.texteditor.ui;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class FiltroText extends FileFilter {

    private static final String TXT = "txt";
    private static final char PUNTO = '.';

    @Override
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        } else {
            return extension(f).equalsIgnoreCase(TXT);
        }
    }

    @Override
    public String getDescription() {
        return "Archivo de texto";
    }

    private String extension(File f) {
        String nombre = f.getName();
        int loc = nombre.lastIndexOf(PUNTO);

        if (loc > 0 && loc < nombre.length() - 1) {
            return nombre.substring(loc + 1);
        } else {
            return "";
        }

    }
}
