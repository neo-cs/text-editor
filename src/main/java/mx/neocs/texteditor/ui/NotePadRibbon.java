/*
 * Copyright 2016 Freddy Barrera (freddy.barrera.moo@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mx.neocs.texteditor.ui;

import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.icon.ImageWrapperResizableIcon;
import org.pushingpixels.flamingo.api.ribbon.JRibbonFrame;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary;

/**
 *
 * @author Freddy Barrera (freddy.barrera@gmail.com)
 */
public class NotePadRibbon extends JRibbonFrame {
    
    private static final long serialVersionUID = 6899339568457673077L;
    
    public NotePadRibbon(String title) {
        super(title);
        init();
    }
    
    private void init() {
        RibbonApplicationMenu menu = new RibbonApplicationMenu();
        ImageIcon imageIcon = ImagesConstants.ICON_APP.getImageIcon();

        ImageWrapperResizableIcon icon = ImageWrapperResizableIcon.getIcon(
                imageIcon.getImage(),
                new Dimension(32, 32));
        ImageWrapperResizableIcon icon2 = ImageWrapperResizableIcon.getIcon(
                imageIcon.getImage(),
                new Dimension(48, 48));
        menu.addMenuEntry(new RibbonApplicationMenuEntryPrimary(icon, "Entrada uno", null, JCommandButton.CommandButtonKind.POPUP_ONLY));

        getRibbon().setApplicationMenu(menu);

        setIconImage(imageIcon.getImage());
        setApplicationIcon(icon2);
        setSize(new Dimension(400, 500));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
