/*
 *   Copyright (C) 2011 Kjell Dirdal All Rights Reserved.
 *  
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; either version 2
 *   of the License, or Mozilla Public License any later version.
 *  
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *  
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *  
 *   -Redistribution of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *  
 *   -Redistribution in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  
 *   Neither the name of Freddy or the names of contributors may
 *   be used to endorse or promote products derived from this software without
 *   specific prior written permission.
 *  
 *   This software is provided "AS IS," without a warranty of any kind. ALL
 *   EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING
 *   ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 *   OR NON-INFRINGEMENT, ARE HEREBY EXCLUDED. Freddy
 *   AND ITS LICENSORS SHALL NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE
 *   AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
 *   DERIVATIVES. IN NO EVENT WILL Freddy OR ITS LICENSORS BE LIABLE FOR ANY LOST
 *   REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL,
 *   INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE THEORY
 *   OF LIABILITY, ARISING OUT OF THE USE OF OR INABILITY TO USE THIS SOFTWARE,
 *   EVEN IF Freddy HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *  
 *   You acknowledge that this software is not designed, licensed or intended
 *   for use in the design, construction, operation or maintenance of any
 *   nuclear facility.
 * 
 */
package mx.neocs.texteditor.ui;

import java.awt.Dimension;
import java.awt.Insets;
import javax.swing.DefaultDesktopManager;
import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JViewport;

/**
 *
 * @author Kjell Dirdal
 */
public class MDIDesktopManager extends DefaultDesktopManager {

    private static final long serialVersionUID = 2213068175684100832L;

    private final JDesktopPane desktop;

    public MDIDesktopManager(JDesktopPane desktop) {
        this.desktop = desktop;
    }

    @Override
    public void endResizingFrame(JComponent f) {
        super.endResizingFrame(f);
        resizeDesktop();
    }

    @Override
    public void endDraggingFrame(JComponent f) {
        super.endDraggingFrame(f);
        resizeDesktop();
    }

    public void setNormalSize() {
        JScrollPane scrollPane = getScrollPane();
        int x = 0;
        int y = 0;
        Insets scrollInsets = getScrollPaneInsets();

        if (scrollPane != null) {
            Dimension d = scrollPane.getVisibleRect().getSize();
            if (scrollPane.getBorder() != null) {
                d.setSize(d.getWidth() - scrollInsets.left - scrollInsets.right, d.getHeight()
                        - scrollInsets.top - scrollInsets.bottom);
            }

            d.setSize(d.getWidth() - 20, d.getHeight() - 20);
            desktop.setMinimumSize(new Dimension(x, y));
            desktop.setMaximumSize(new Dimension(x, y));
            desktop.setPreferredSize(new Dimension(x, y));
            scrollPane.invalidate();
            scrollPane.validate();
        }
    }

    private Insets getScrollPaneInsets() {
        JScrollPane scrollPane = getScrollPane();
        if (scrollPane == null) {
            return new Insets(0, 0, 0, 0);
        } else {
            return getScrollPane().getBorder().getBorderInsets(scrollPane);
        }
    }

    private JScrollPane getScrollPane() {
        if (desktop.getParent() instanceof JViewport) {
            JViewport viewPort = (JViewport) desktop.getParent();
            if (viewPort.getParent() instanceof JScrollPane) {
                return (JScrollPane) viewPort.getParent();
            }
        }
        return null;
    }

    protected void resizeDesktop() {
        int x = 0;
        int y = 0;
        JScrollPane scrollPane = getScrollPane();
        Insets scrollInsets = getScrollPaneInsets();

        if (scrollPane != null) {
            JInternalFrame allFrames[] = desktop.getAllFrames();

            for (JInternalFrame allFrame : allFrames) {
                if (allFrame.getX() + allFrame.getWidth() > x) {
                    x = allFrame.getX() + allFrame.getWidth();
                }
                if (allFrame.getY() + allFrame.getHeight() > y) {
                    y = allFrame.getY() + allFrame.getHeight();
                }
            }
            Dimension d = scrollPane.getVisibleRect().getSize();
            if (scrollPane.getBorder() != null) {
                d.setSize(d.getWidth() - scrollInsets.left - scrollInsets.right, d.getHeight()
                        - scrollInsets.top - scrollInsets.bottom);
            }

            if (x <= d.getWidth()) {
                x = ((int) d.getWidth()) - 20;
            }

            if (y <= d.getHeight()) {
                y = ((int) d.getHeight()) - 20;
            }

            //desktop.setAllSize(x, y);
            desktop.setMinimumSize(new Dimension(x, y));
            desktop.setMaximumSize(new Dimension(x, y));
            desktop.setPreferredSize(new Dimension(x, y));
            scrollPane.invalidate();
            scrollPane.validate();
        }
    }
}
