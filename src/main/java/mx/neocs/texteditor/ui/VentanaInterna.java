/*
 * Copyright 2016 Freddy Barrera (freddy.barrera.moo@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mx.neocs.texteditor.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DropMode;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.event.InternalFrameEvent;

/**
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class VentanaInterna extends JInternalFrame {

    private static final long serialVersionUID = 4841378985557627079L;
    private static final Logger LOGGER = Logger.getLogger(VentanaInterna.class.getName());

    private File archivo;
    private JEditorPane jep;
    private JScrollPane jsp;
    private String textoOriginal;

    public VentanaInterna(String title) {
        this(title, null);
    }

    public VentanaInterna(String titulo, File archivo) {
        super(titulo, true, true, true, true);
        this.archivo = archivo;
        init();
    }
// Open big files http://stackoverflow.com/questions/25048674/how-to-read-and-display-large-text-files-in-swing
    private void init() {
        jep = new JEditorPane();
        jsp = new JScrollPane();

        try (FileInputStream fis = new FileInputStream(archivo = (archivo == null ? File.createTempFile("nuevo", ".~txt") : archivo));
                Scanner scanner = new Scanner(fis)) {
            StringBuilder sb = new StringBuilder();
            
            while(scanner.hasNextLine()) {
                sb.append(scanner.nextLine());
            }
            
            if (scanner.ioException() != null) {
                throw scanner.ioException();
            }
            
            textoOriginal = sb.toString();
            jep.setDoubleBuffered(true);
            jep.setText(sb.toString());
            jep.setDragEnabled(true);
            jep.setDropMode(DropMode.INSERT);

            jsp.setViewportView(jep);

            setSize(400, 400);
            add(jsp);
            addInternalFrameListener(new Adaptador());
        } catch (IOException ex) {
            LOGGER.severe(ex.getMessage());
        }
    }

    public String getText() {
        return jep.getText();
    }

    private void guardarArchivo(File archivo, String contenido) {
        try (Writer w = new FileWriter(archivo)) {
            w.write(contenido);
        } catch (IOException ex) {
            LOGGER.severe(ex.getMessage());
        }
    }

    private class Adaptador extends javax.swing.event.InternalFrameAdapter {

        @Override
        public void internalFrameClosing(InternalFrameEvent e) {
            if (jep.getText().equals(textoOriginal)) {
                super.internalFrameClosing(e);
            } else {
                int opcion = JOptionPane.showInternalConfirmDialog(e.getInternalFrame(),
                        "Los cambios no se han guardado, ¿Desea guardar los cambios, antes de salir?",
                        "Desea guargar los cambios", JOptionPane.YES_NO_OPTION);
                if (JOptionPane.YES_OPTION == opcion) {
                    JFileChooser dialogoGuardar = new JFileChooser();
                    opcion = dialogoGuardar.showSaveDialog(e.getInternalFrame());

                    if (opcion != JFileChooser.CANCEL_OPTION) {
                        File archivo = dialogoGuardar.getSelectedFile();

                        if (!archivo.getName().endsWith(".txt")) {
                            try {
                                String path = archivo.getCanonicalPath() + ".txt";
                                File destino = new File(path);
                                archivo = destino;
                            } catch (IOException ex) {
                                LOGGER.severe(ex.getMessage());
                            }
                        }

                        guardarArchivo(archivo, jep.getText());
                        super.internalFrameClosing(e);
                    } else {
                        super.internalFrameClosing(e);
                    }

                } else {
                    super.internalFrameClosing(e);
                }
            }
        }
    }
}
