/*
 * Copyright 2016 Freddy Barrera (freddy.barrera.moo@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mx.neocs.texteditor.ui;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;

/**
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class NotePad extends javax.swing.JFrame {

    private static final long serialVersionUID = -5456635815620391047L;

//TODO Hacer que el menu ventana active y desactive dinamicamente las ventanas internas

    public NotePad() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFileChooserOpen = new javax.swing.JFileChooser();
        jFileChooserSave = new javax.swing.JFileChooser();
        jToolBar1 = new javax.swing.JToolBar("Barra de herramientas");
        jButtonNew = new javax.swing.JButton();
        jButtonOpen = new javax.swing.JButton();
        jButtonSave = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jDesktopPane1 = new javax.swing.JDesktopPane();
        jMenuBar = new javax.swing.JMenuBar();
        jMenuFile = new javax.swing.JMenu();
        jMenuNew = new javax.swing.JMenuItem();
        jMenuOpen = new javax.swing.JMenuItem();
        jMenuSave = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuExit = new javax.swing.JMenuItem();
        jMenuWindow = new javax.swing.JMenu();

        jFileChooserOpen.setFileFilter(new FiltroText());

        jFileChooserSave.setDialogType(javax.swing.JFileChooser.SAVE_DIALOG);
        jFileChooserSave.setFileFilter(new FiltroText());

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("mx/neocs/texteditor/resources/i18n/i18nDefaults"); // NOI18N
        setTitle(bundle.getString("App.Title")); // NOI18N
        setLocationByPlatform(true);

        jToolBar1.setRollover(true);

        jButtonNew.setText(bundle.getString("Button.New")); // NOI18N
        jButtonNew.setFocusable(false);
        jButtonNew.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonNew.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newFileActionPerformed(evt);
            }
        });
        jToolBar1.add(jButtonNew);

        jButtonOpen.setText(bundle.getString("Button.Open")); // NOI18N
        jButtonOpen.setToolTipText(bundle.getString("Button.ToolTip.Open")); // NOI18N
        jButtonOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openFileActionPerformed(evt);
            }
        });
        jToolBar1.add(jButtonOpen);

        jButtonSave.setText(bundle.getString("Button.Save")); // NOI18N
        jButtonSave.setFocusable(false);
        jButtonSave.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonSave.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveFileActionPerformed(evt);
            }
        });
        jToolBar1.add(jButtonSave);

        getContentPane().add(jToolBar1, java.awt.BorderLayout.PAGE_START);

        jDesktopPane1.setDesktopManager(new MDIDesktopManager(jDesktopPane1));
        jScrollPane1.setViewportView(jDesktopPane1);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jMenuFile.setMnemonic(java.util.ResourceBundle.getBundle("mx/neocs/texteditor/resources/i18n/mnemonics").getString("Menu.File").charAt(0));
        jMenuFile.setText(bundle.getString("Menu.File")); // NOI18N

        jMenuNew.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        jMenuNew.setText(bundle.getString("Menu.FileNew")); // NOI18N
        jMenuNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newFileActionPerformed(evt);
            }
        });
        jMenuFile.add(jMenuNew);

        jMenuOpen.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        jMenuOpen.setMnemonic(java.util.ResourceBundle.getBundle("mx/neocs/texteditor/resources/i18n/mnemonics").getString("Menu.FileOpen").charAt(0));
        jMenuOpen.setText(bundle.getString("Menu.FileOpen")); // NOI18N
        jMenuOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openFileActionPerformed(evt);
            }
        });
        jMenuFile.add(jMenuOpen);

        jMenuSave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMenuSave.setText(bundle.getString("Menu.FileSave")); // NOI18N
        jMenuSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveFileActionPerformed(evt);
            }
        });
        jMenuFile.add(jMenuSave);
        jMenuFile.add(jSeparator1);

        jMenuExit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        jMenuExit.setText(bundle.getString("Menu.FileExit")); // NOI18N
        jMenuExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitAppActionPerformed(evt);
            }
        });
        jMenuFile.add(jMenuExit);

        jMenuBar.add(jMenuFile);

        jMenuWindow.setText(bundle.getString("Menu.Window")); // NOI18N
        jMenuBar.add(jMenuWindow);

        setJMenuBar(jMenuBar);

        setSize(new java.awt.Dimension(421, 336));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void openFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openFileActionPerformed
        int accion = jFileChooserOpen.showDialog(this, java.util.ResourceBundle.getBundle("mx/neocs/texteditor/resources/i18n/i18nDefaults").getString("FileChooser.Open"));

        if (accion != JFileChooser.CANCEL_OPTION) {
            File file = jFileChooserOpen.getSelectedFile();
            String titulo = file.getName();
            JMenuItem menu = new JMenuItem(titulo);

            jMenuWindow.add(menu);
            VentanaInterna ve = new VentanaInterna(titulo, file);
            jDesktopPane1.add(ve);
            ve.setVisible(true);
        }
    }//GEN-LAST:event_openFileActionPerformed

    private void saveFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveFileActionPerformed
        int showSaveDialog = jFileChooserSave.showSaveDialog(this);

        if (showSaveDialog != JFileChooser.CANCEL_OPTION) {
            File file = jFileChooserSave.getSelectedFile();

            if (!file.getName().endsWith(".txt")) {
                try {
                    String path = file.getCanonicalPath() + ".txt";
                    File dest = new File(path);
                    file = dest;
                } catch (IOException ex) {
                    Logger.getLogger(NotePad.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                }
            }

            try (Writer w = new FileWriter(file)) {
                VentanaInterna v = (VentanaInterna)jDesktopPane1.getSelectedFrame();
                w.write(v.getText());
                
            } catch (IOException ex) {
                Logger.getLogger(NotePad.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
        }

    }//GEN-LAST:event_saveFileActionPerformed

    private void exitAppActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitAppActionPerformed
        System.exit(0);
    }//GEN-LAST:event_exitAppActionPerformed

    private void newFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newFileActionPerformed
        VentanaInterna ve = new VentanaInterna("Nuevo");
        jDesktopPane1.add(ve);
        ve.setVisible(true);
    }//GEN-LAST:event_newFileActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonNew;
    private javax.swing.JButton jButtonOpen;
    private javax.swing.JButton jButtonSave;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JFileChooser jFileChooserOpen;
    private javax.swing.JFileChooser jFileChooserSave;
    private javax.swing.JMenuBar jMenuBar;
    private javax.swing.JMenuItem jMenuExit;
    private javax.swing.JMenu jMenuFile;
    private javax.swing.JMenuItem jMenuNew;
    private javax.swing.JMenuItem jMenuOpen;
    private javax.swing.JMenuItem jMenuSave;
    private javax.swing.JMenu jMenuWindow;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JToolBar jToolBar1;
    // End of variables declaration//GEN-END:variables

}
