/*
 * Copyright 2017 Freddy Barrera (freddy.barrera@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mx.neocs.texteditor.util;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Freddy Barrera (freddy.barrera@gmail.com)
 */
public class FileTransformer {

    private static final Logger LOG = Logger.getLogger(FileTransformer.class.getName());
    
    public static final Charset WINDOWS_LATIN_CHARSET = Charset.forName("windows-1252");
    public static final Charset MS_DOS_LATIN_CHARSET = Charset.forName("Cp850");
    public static final Charset UTF8_CHARSET = Charset.forName("UTF-8");
    public static final String UNIX_LINE_SEPARATOR = "\n";
    public static final String MAC_OS_LINE_SEPARATOR = "\r";
    public static final String WINDOWS_LINE_SEPARATOR = "\r\n";
    
    private FileTransformer() {}
    
    public static void encodeFileAs(Path filePath, Charset charsetSource, Charset charsetTarget) {
        try {
            List<String> fileLines = Files.readAllLines(filePath, charsetSource);
            Files.delete(filePath);
            Path filePathOut = Paths.get("/home/neo_cs/VirtualBox VMs/Share/salida.txt");
            Files.write(filePathOut, fileLines, charsetTarget);
            fileLines = null;
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }
}
