/*
 * Copyright 2017 Freddy Barrera (freddy.barrera@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mx.neocs.texteditor.util;

import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Freddy Barrera (freddy.barrera@gmail.com)
 */
public class FileTransnfomerTest {
    
    public FileTransnfomerTest() {
    }

    @Test
    public void encode() {
        Path path = Paths.get("/home/neo_cs/Facebook - Dimensions of my page s profile picture and cover photo.txt");
        FileTransformer.encodeFileAs(path, FileTransformer.UTF8_CHARSET, FileTransformer.WINDOWS_LATIN_CHARSET);
    }
}
