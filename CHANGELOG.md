# Release notes
* Convert to Maven project
* Upgraded from JDK 1.6 to JDK 1.8
* License changed to Apache 2.0
